﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SignmageSDK;

namespace EjemplosSDK
{
    class Sticker
    {
        SignmageSDK.FirmamexServices signmageServices;
        String base64;
        String name;

        public Sticker(SignmageSDK.FirmamexServices signmageServices, String base64, String name) {
            this.signmageServices = signmageServices;
            this.base64 = base64;
            this.name = name;
        }

        public String request()
        {
            B64_doc b64_doc = new B64_doc { name = name, data = base64 };

            JObject stickerEjemplo = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                //jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3OTc3IDIwNTEiLCJpc3MiOiJmaXJtYW1leCIsImF1ZCI6ImFkZGluIn0.xjl8Al2cQmbad0xYeEkq_LRa89hgRp0UiGc7ZcbSb2E"
                stickers = new object[] {
                    new {
                        authority = "SAT",
                        stickerType = "line",
                        dataType = "rfc",
                        data = "ARCX9012226P8",
                        imageType = "hash",
                        email = "jhon@gmail.com",
                        page = "0",
                        rect = new Rect { lx = 100f, ly = 100f, tx = 200f, ty = 200f }
                    }
                },
                app2 = true
            });

            String stickerParams = stickerEjemplo.ToString(Formatting.None);
            return signmageServices.request(stickerParams);
        }
    }
}
