﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class Expediente
    {
        SignmageSDK.FirmamexServices signmageServices;
        String base64;
        String name;

        public Expediente(SignmageSDK.FirmamexServices signmageServices, String base64, String name)
        {
            this.signmageServices = signmageServices;
            this.base64 = base64;
            this.name = name;
        }

        public String request(String nombreExpediente)
        {
            JObject createDocumentSetEjemplo = JObject.FromObject(new
            {
                name = nombreExpediente,
                metadata = new JObject { } // metadata es opcional
            });

            String createDocumentSetParams = createDocumentSetEjemplo.ToString(Formatting.None);
            String documentSet = signmageServices.createDocumentSet(createDocumentSetParams);

            DocumentSet documentSetObj = JsonConvert.DeserializeObject<DocumentSet>(documentSet);

            B64_doc b64_doc = new B64_doc { name = name, data = base64 };

            JObject stickerExampleWithDocumentSet = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                documentSet = documentSetObj.document_set,
                stickers = new object[] {
                    new {
                        authority = "SAT",
                        stickerType = "line",
                        dataType = "rfc",
                        data = "ARCX9012226P8",
                        imageType = "hash",
                        email = "jhon@gmail.com",
                        page = "0",
                        rect = new Rect { lx = 100f, ly = 100f, tx = 200f, ty = 200f }
                    }
                },
                app2 = true
            });
            
            String stickerParams = stickerExampleWithDocumentSet.ToString(Formatting.None);
            String stickerResponse = signmageServices.request(stickerParams);

            JObject closeDocumentSetEjemplo = JObject.FromObject(new
            {
                documentSet = documentSetObj.document_set,
                workflow = new object[] {
                    new {
                        remind_every = "1d",
                        language = "es",
                        ordered = new object[]
                        {
                            "ARCX9012226P8"
                        }
                    }
                }
            });

            String closeDocumentSetParams = closeDocumentSetEjemplo.ToString(Formatting.None);
            return signmageServices.request(closeDocumentSetParams);
        }
    }
}
