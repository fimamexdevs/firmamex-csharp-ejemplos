using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EjemplosSDK
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] textboxPDF = File.ReadAllBytes("C:\\doc.pdf"); // Ubicacion documento con recuadro de firma
            string base64Textbox = Convert.ToBase64String(textboxPDF); // O directamente el base64 del pdf de recuadro

            byte[] pdf = File.ReadAllBytes("C:\\doc.pdf"); // Ubicacion del documento donde se aplicara el sticker+flujo
            string base64pdf = Convert.ToBase64String(pdf); // O directamente el base64 del pdf para sticker+flujo
            
            SignmageSDK.FirmamexServices signmageSDK = new SignmageSDK.FirmamexServices("FrYaLVeRDIX0TPpy", "90b9b52e56e6213e76fa1199f6d4602b"); // webId, apiKey

            EditFlow editFlow = new EditFlow(signmageSDK, base64pdf, "PDF");
            //JObject response = editFlow.request();

            // Documento con sticker
            Sticker stickerEjemplo = new Sticker(signmageSDK, base64pdf, "PDF");
            //String response = stickerEjemplo.request();

            // Documento con sticker+flujo
            Flow flujoEjemplo = new Flow(signmageSDK, base64pdf, "PDF");
            //String response = flujoEjemplo.request();

            // Documento que tenga recuadro de firma
            Textbox textboxEjemplo = new Textbox(signmageSDK, base64Textbox, "textboxPDF");
            //String response = textboxEjemplo.request();

            // Documento generado con un template ya existente
            Template templateEjemplo = new Template(signmageSDK);
            //String response = templateEjemplo.request();

            // Creando un template nuevo
            UploadTemplate uploadTemplate = new UploadTemplate(signmageSDK);
            //String response = uploadTemplate.request("<html><style>thead,    table {        border: 1px solid black;        border-collapse: collapse;        letter-spacing: 1px;    }    th {                padding-top: 12px;        padding-bottom: 12px;        text-align: left;        background-color: #4CAF50;        color: white;    }    td {        padding: 10px;        border: 1px solid black;        text-align: center;    }    td, th {        border: 1px solid #ddd;        padding: 8px;    }    table tr:nth-child(even){background-color: #f2f2f2;}</style><body>    <div style=\"margin - left: 50px; \">        <p style=\"text - align:justify; \">            <div style=\"\">                <span>Codigo: </span>                <span class=\"signmage - template - field\" id=\"codigo\"></span>            </div>        </p>        <p style=\"text - align:justify; \">            <div style=\"\">                <span>Proveedor: </span>                <span class=\"signmage - template - field\" id =\"proveedor\"></span>            </div>        </p>        <p style=\"text - align:justify; \">            <span>Empresa: </span>            <span class=\"signmage - template - field\" id=\"empresa\"></span>        </p>        <table class=\"signmage - template - table\" id=\"directory\" style=\"width: 100 %; \">            <thead style=\"style = \"border-bottom: 1px solid black\" >                < tr style = \"height: 31px;\" >                    < th >                        < strong > Clave </ strong >                    </ th >                    < th >                        < strong > Descripción </ strong >                    </ th >                    < th >                        < strong > Cantidad </ strong >                    </ th >                    < th >                        < strong > Precio </ strong >                    </ th >                    < th >                        < strong > Total </ strong >                    </ th >                </ tr >            </ thead >            < tbody >            </ tbody >        </ table >        < br />                </ div >    < div style = \"border:1px solid black; float:left; width:200px; height:120px; margin:auto; color:white; word-break:break-all; font-size:2px;\" >        < span class=\"signmage-template-field\" id=\"sign_1\"></span>    </div>    <div style = \"border:1px solid black; float:right; width:200px; height:120px; margin:auto; color:white; word-break:break-all; font-size:2px;\" >        < span class=\"signmage-template-field\" id=\"sign_2\"></span>    </div></body></html>");

            // Solicitud para obtener informacion de un documento
            GetReport getReport = new GetReport(signmageSDK);
            //JObject response = getReport.request("");

            // Obtiene un documento en base el id de una notificacion
            GetData getData = new GetData(signmageSDK);
            //String response = getData.request("0");

            GetDocument getDocument = new GetDocument(signmageSDK);
            //JObject response = getDocument.request("", "");

            //Expediente expediente = new Expediente(signmageSDK, base64pdf, "PDF");
            //String documentSet = expediente.request("nombre6");

            JObject createDocumentSetEjemplo = JObject.FromObject(new
            {
                name = "ddd",
                metadata = new JObject { } // metadata es opcional
            });

            String createDocumentSetParams = createDocumentSetEjemplo.ToString(Formatting.None);
            String documentSet = signmageSDK.createDocumentSet(createDocumentSetParams);

            DocumentSet documentSetObj = JsonConvert.DeserializeObject<DocumentSet>(documentSet);

            B64_doc b64_doc = new B64_doc { name = "PDF", data = base64pdf };

            JObject stickerExampleWithDocumentSet = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                document_set = documentSetObj.document_set,
                stickers = new object[] {
                    new {
                        authority = "SAT",
                        stickerType = "line",
                        dataType = "rfc",
                        data = "ARCX9012226P8",
                        imageType = "hash",
                        email = "jhon@gmail.com",
                        page = "0",
                        rect = new Rect { lx = 100f, ly = 100f, tx = 200f, ty = 200f }
                    }
                },
                app2 = true
            });

            String stickerParams = stickerExampleWithDocumentSet.ToString(Formatting.None);
            String stickerResponse = signmageSDK.request(stickerParams);

            JObject closeDocumentSetEjemplo = JObject.FromObject(new
            {
                documentSet = documentSetObj.document_set,
                workflow = new {
                    remind_every = "1d",
                    language = "es",
                    ordered = new string[]{"ARCX9012226P8" }
                }
            });

            String closeDocumentSetParams = closeDocumentSetEjemplo.ToString(Formatting.None);
            String responseClose = signmageSDK.closeDocumentSet(closeDocumentSetParams);

            Console.ReadKey();
        }
    }
}
