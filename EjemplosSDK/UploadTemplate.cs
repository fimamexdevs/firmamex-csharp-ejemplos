﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class UploadTemplate
    {
        SignmageSDK.FirmamexServices signmageServices;

        public UploadTemplate(SignmageSDK.FirmamexServices signmageServices)
        {
            this.signmageServices = signmageServices;
        }

        public String request(String html) {

            var bytes = System.Text.Encoding.UTF8.GetBytes(html);

            JObject uploadTemplate = JObject.FromObject(new
            {
                template_type = "html",
                template_data = Convert.ToBase64String(bytes),
                template_name = "template_example20"
            });

            String uploadParams = uploadTemplate.ToString(Formatting.None);
            return signmageServices.saveTemplate(uploadParams);
        }
    }
}
