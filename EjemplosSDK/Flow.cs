﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class Flow
    {
        SignmageSDK.FirmamexServices signmageServices;
        String base64;
        String name;

        public Flow(SignmageSDK.FirmamexServices signmageServices, String base64, String name)
        {
            this.signmageServices = signmageServices;
            this.base64 = base64;
            this.name = name;
        }

        public String request()
        {
            B64_doc b64_doc = new B64_doc { name = name, data = base64 };

            JObject flujoEjemplo = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                stickers = new object[] {
                    new {
                        authority = "SAT",
                        stickerType = "line",
                        dataType = "rfc",
                        data = "ARCX9012226P8",
                        imageType = "hash",
                        email = "jhon@gmail.com",
                        page = "0",
                        rect = new Rect { lx = 226.45f, ly = 355.25f, tx = 397.75f, ty = 413.85f }
                    }
                },
                workflow = new object[] {
                    new {
                        data = "ARCX9012226P8"
                    }
                },
                app2 = true
            });

            String flujoParams = flujoEjemplo.ToString(Formatting.None);
            return signmageServices.request(flujoParams);
        }
    }
}
