﻿﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class Template
    {
        SignmageSDK.FirmamexServices signmageServices;

        public Template(SignmageSDK.FirmamexServices signmageServices)
        {
            this.signmageServices = signmageServices;
        }

        public String request()
        {
            JObject templateEjemplo = JObject.FromObject(new
            {
                template_title = "test",
                fields = new object[] {
                    new {
                        id = "firmante", value = "Firmamex juanperez@gmail.com liga dibujo linea"
                    }, 
                    new {
                        id = "nombre", value = "Juan Perez"
                    },
                    new {
                        id = "empresa", value = "ACME S.A. de C.V."
                    }

                },
                app2 = true
            });

            String templateParams = templateEjemplo.ToString(Formatting.None);
            return signmageServices.request(templateParams);
        }
    }
}
