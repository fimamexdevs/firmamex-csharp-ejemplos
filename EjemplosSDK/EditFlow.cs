﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SignmageSDK;

namespace EjemplosSDK
{
    class EditFlow
    {
        SignmageSDK.FirmamexServices firmamexServices;
        String base64;
        String name;

        public EditFlow(SignmageSDK.FirmamexServices firmamexServices, String base64, String name)
        {
            this.firmamexServices = firmamexServices;
            this.base64 = base64;
            this.name = name;
        }

        public JObject request()
        {
            B64_doc b64_doc = new B64_doc { name = name, data = base64 };

            JObject stickerEjemplo = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                stickers = new object[] {
                    new {
                        authority = "SAT",
                        stickerType = "line",
                        dataType = "rfc",
                        data = "GOCF9002226A7",
                        imageType = "hash",
                        email = "fernando@firmamex.com",
                        page = "0",
                        rect = new Rect { lx = 100f, ly = 100f, tx = 200f, ty = 200f }
                    }
                },
                app2 = true
            });

            String stickerParams = stickerEjemplo.ToString(Formatting.None);
            JObject document = JObject.Parse(firmamexServices.request(stickerParams));

            return firmamexServices.workflow(document.Value<String>("document_ticket"), JObject.FromObject(
                new
                {
                    expiration_date = "30/11/2022"
                }
                ));
        }
    }
}
