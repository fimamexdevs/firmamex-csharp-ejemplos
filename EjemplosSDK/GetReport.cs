﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class GetReport
    {
        SignmageSDK.FirmamexServices signmageServices;

        public GetReport(SignmageSDK.FirmamexServices signmageServices)
        {
            this.signmageServices = signmageServices;
        }

        public JObject request(String ticket) {
            return signmageServices.getAPIReport(ticket);
        }
    }
}
