﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class GetDocument
    {
        SignmageSDK.FirmamexServices signmageServices;

        public GetDocument(SignmageSDK.FirmamexServices signmageServices)
        {
            this.signmageServices = signmageServices;
        }

        public JObject request(String type, String ticket)
        {
            return signmageServices.getDocument(type, ticket);
        }
    }
}
