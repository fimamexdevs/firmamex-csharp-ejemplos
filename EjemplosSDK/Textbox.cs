﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class Textbox
    {
        SignmageSDK.FirmamexServices signmageServices;
        String base64;
        String name;

        public Textbox(SignmageSDK.FirmamexServices signmageServices, String base64, String name)
        {
            this.signmageServices = signmageServices;
            this.base64 = base64;
            this.name = name;
        }

        public String request()
        {
            B64_doc b64_doc = new B64_doc { name = name, data = base64 };

            JObject recuadroEjemplo = JObject.FromObject(new
            {
                b64_doc = b64_doc,
                app2 = true
            });

            String recuadroParams = recuadroEjemplo.ToString(Formatting.None);
            return signmageServices.request(recuadroParams);
        }
    }
}
