﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK.JsonObjects
{
    class Sticker
    {
        public String authority { get; set; }
        public String stickerType { get; set; }
        public String dataType { get; set; }
        public String data { get; set; }
        public String imageType { get; set; }
        public String email { get; set; }
        public String page { get; set; }
        public Rect rect { get; set; }
    }
}
