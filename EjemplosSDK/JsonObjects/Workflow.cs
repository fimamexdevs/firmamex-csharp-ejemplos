﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK.JsonObjects
{
    class Workflow
    {

        public String expiration_date { get; set; }
        public String remind_every { get; set; }
        public String language { get; set; }
        public Ordered[] ordered { get; set; }
    }
}
