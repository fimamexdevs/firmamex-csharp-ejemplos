﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK.JsonObjects
{
    class Rect
    {
        public float lx { get; set; }
        public float ly { get; set; }
        public float tx { get; set; }
        public float ty { get; set; }
    }
}
