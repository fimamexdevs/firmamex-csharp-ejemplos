﻿using EjemplosSDK.JsonObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemplosSDK
{
    class GetData
    {
        SignmageSDK.FirmamexServices signmageServices;

        public GetData(SignmageSDK.FirmamexServices signmageServices)
        {
            this.signmageServices = signmageServices;
        }

        public String request(String notificationId)
        {
            JObject getDataRequest = JObject.FromObject(new
            {
                notification_id = notificationId,
                action = "download_original_bytes"
            });

            String getDataParams = getDataRequest.ToString(Formatting.None);
            return signmageServices.getData(getDataParams);
        }
    }
}
